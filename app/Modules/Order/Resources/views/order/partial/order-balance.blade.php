   <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Sub Total :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-cash4 "></i></span>
                        </span>
                        {!! Form::text('sub_total', $value = null, ['id'=>'sub_total','placeholder'=>'Sub Total','class'=>'sub_total form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>

   <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Point Amount :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-cash4 "></i></span>
                        </span>
                        {!! Form::text('point_amount', $value = null, ['id'=>'point_amount','placeholder'=>'Use Point Amount','class'=>'point_amount form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>


    <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">GST (6% +):</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-cash4 "></i></span>
                        </span>
                        {!! Form::text('gst_amount', $value = null, ['id'=>'gst_amount','placeholder'=>'GST Amount','class'=>'gst_amount form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>

    <div class="mb-1 row">
        <div class="col-lg-8"></div>
          <div class="col-lg-4">
                <div class="row">
                  <label class="col-form-label col-lg-4">Grand Total :</label>
                      <div class="col-lg-8 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                          <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-cash4 "></i></span>
                        </span>
                        {!! Form::text('grand_total', $value = null, ['id'=>'grand_total','placeholder'=>'Grand Total','class'=>'grand_total form-control numeric','readonly'=>'readonly']) !!}
                    </div>
                </div>
            </div>
          </div>
    </div>
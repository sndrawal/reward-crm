@extends('admin::layout')
@section('title')Customer @stop
@section('breadcrum')Customer @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>

@stop
@section('content')

<div class="card card-body">
    <div class="d-flex justify-content-between mb-4">
        <h4>List Of Customers</h4>
        <div class="button-group"> 
            <a href="{{route('reward.index')}}" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-flip-horizontal2"></i></b> Back To Reward </a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr class="bg-slate">
                    <th>S.N.</th>
                    <th>Customer Name</th>
                    <th>Mobile No.</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Point</th>
                    <th>Point Amount</th>
                    <th>Expiry Date</th>
                </tr>
            </thead>
            <tbody>
                @if($customer->total() != 0)
                @foreach($customer as $key => $value)
                <tr>
                    <td>{{$customer->firstItem() +$key}}</td>
                    <td>{{ $value->customer_name }}</td>
                    <td>{{ $value->mobile_no }}</td>
                    <td>{{ $value->address }}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->point }}</td>
                    <td>{{ number_format($value->point_amount,2) }}</td>
                    <td>{{$value->expiry_date}}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">No Customer Found !!!</td>
                </tr>
                @endif
            </tbody>

        </table>
        <span style="margin: 5px;float: right;">
            @if($customer->total() != 0)
                {{ $customer->links() }}
            @endif
            </span>
    </div>
</div>

@endsection

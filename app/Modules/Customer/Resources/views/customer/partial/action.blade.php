<script src="{{ asset('admin/global/js/plugins/pickers/daterangepicker.js')}}"></script>
<script src="{{ asset('admin/global/js/demo_pages/picker_date.js')}}"></script>
<script src="{{ asset('admin/validation/customer.js')}}"></script>

 <fieldset class="mb-3">
    <legend class="text-uppercase font-size-sm font-weight-bold">Customer Information</legend>

    <div class="row">

         <div class="col-md-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Customer Name:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-store2"></i>
                                </span>
                            </span>
                             {!! Form::text('customer_name', $value = null, ['placeholder'=>'Enter Customer Name','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

         <div class="col-md-6">
            <div class="form-group row">
                <label class="col-form-label col-lg-3">Mobile No:<span class="text-danger">*</span></label>
                    <div class="col-lg-9">
                       <div class="input-group">
                        <span class="input-group-prepend">
                            <span class="input-group-text"><i class="icon-location4"></i></span>
                        </span>
                        {!! Form::text('mobile_no', $value = null, ['placeholder'=>'Enter Mobile No','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row">

         <div class="col-md-6">
            <div class="row">
                <label class="col-form-label col-lg-3">Address:<span class="text-danger">*</span></label>
                    <div class="col-lg-9 form-group-feedback form-group-feedback-right">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-vcard"></i>
                                </span>
                            </span>
                             {!! Form::text('address', $value = null, ['placeholder'=>'Enter Address','class'=>'form-control']) !!}
                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-6">

        <div class="form-group row">
            <label class="col-form-label col-lg-3">Email:<span class="text-danger">*</span></label>
                <div class="col-lg-9">
                   <div class="input-group">
                    <span class="input-group-prepend">
                        <span class="input-group-text"><i class="icon-phone"></i></span>
                    </span>
                    {!! Form::email('email', $value = null, ['placeholder'=>'Enter Email','class'=>'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</fieldset>


<div class="text-right">
     <button type="submit" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left"><b><i class="icon-database-insert"></i></b>{{ $btnType }}</button>
</div>

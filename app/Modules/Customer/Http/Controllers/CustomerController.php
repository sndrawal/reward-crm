<?php

namespace App\Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Modules\Customer\Repositories\CustomerInterface;

class CustomerController extends Controller
{

     protected $customer;
    
    public function __construct(CustomerInterface $customer)
    {
        $this->customer = $customer;
    }

     /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    { 

        $search = $request->all();
        $data['customer'] = $this->customer->findAll($limit= 50,$search);
        $data['search_value']=$search;

        return view('customer::customer.index',$data);

    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['is_edit'] = true;
        $data['customer'] = $this->customer->find($id);
        
        return view('customer::customer.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
         try {
            $this->customer->update($id,$data);
            alertify()->success('Customer Updated Successfully');

            } catch (\Throwable $e) {
                alertify($e->getMessage())->error();
            }
       
         return redirect(route('customer.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
         try {
            $this->customer->delete($id);
            alertify()->success('Customer Deleted Successfully');
        } catch (\Throwable $e) {
            alertify($e->getMessage())->error();
        }
        return redirect(route('customer.index'));
    }


}

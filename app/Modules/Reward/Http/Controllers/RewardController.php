<?php

namespace App\Modules\Reward\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Order\Repositories\OrderInterface;
use App\Modules\Order\Repositories\ItemOrderInterface;
use App\Modules\Customer\Repositories\CustomerInterface;
use App\Modules\Item\Repositories\ItemInterface;

class RewardController extends Controller
{
    protected $order;
    protected $item_order;
    protected $items;
    protected $customer;

    public function __construct(OrderInterface $order, ItemOrderInterface $item_order, ItemInterface $items, CustomerInterface $customer)
    {
        $this->order = $order;
        $this->item_order = $item_order;
        $this->items = $items;
        $this->customer = $customer;
    }

    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data['total_customer'] = $this->customer->countTotal();
        $data['total_item'] = $this->items->countTotal();
        $data['no_of_order'] = $this->order->countTotal();
        $data['total_sales_amount'] =$this->item_order->getSalesAmount();

        return view('reward::reward.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('reward::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('reward::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('reward::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}

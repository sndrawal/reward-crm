@extends('admin::layout')
@section('title')Reward Dashboard @stop
@section('breadcrum')Reward Dashboard @stop
@section('content')


<div class="row mt-2">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body text-center">
                <i class="icon-medal-star icon-2x text-pink-400 border-pink-400 border-3 rounded-round p-3 mb-3 mt-1"></i>
                <h5 class="card-title">Reward CRM</h5>
                <p class="mb-3">Related To Order and Sales. Also, Point Award to Customer after Sales Completion.</p>
                <a href="{{route('reward.index')}}" class="btn bg-pink-400">Click Me</a>
            </div>
        </div>
    </div>

</div>

@stop

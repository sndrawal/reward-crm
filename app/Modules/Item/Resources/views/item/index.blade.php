@extends('admin::layout')
@section('title')Item @stop
@section('breadcrum')Item @stop

@section('script')
<script src="{{asset('admin/global/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script src="{{asset('admin/global/js/plugins/forms/selects/select2.min.js')}}"></script>
@stop

@section('content')
<div class="card card-body">
    <div class="d-flex justify-content-between">
        <h4>List of Item</h4>
        <div class="button-group"> 

            <a href="{{route('item.create')}}" class="ml-2 btn bg-success-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-plus2"></i></b> Add Item </a>

            <a href="{{route('reward.index')}}" class="ml-2 btn bg-pink-600 btn-labeled btn-labeled-left" style="float: left"><b><i class="icon-flip-horizontal2"></i></b> Back To Reward </a>
        </div>

    </div>
    <div class="mb-3 mt-3"></div>
    <div class="table-responsive table-card">
        <table class="table table-striped">
            <thead>
                <tr class="bg-slate">
                    <th>#</th>
                    <th>Item</th>
                    <th>Normal Price</th>
                    <th>Promotion Price</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if($item->total() != 0)
                @foreach($item as $key => $value)
                <tr>
                    <td>{{$item->firstItem() +$key}}</td>
                    <td>{{ $value->name }}</td>
                    <td>${{ number_format($value->normal_price,2) }}</td>
                    <td>${{ number_format($value->promotion_price,2) }}</td>

                    <td>
                        <a class="btn bg-info btn-icon rounded-round" href="{{ route('item.edit',$value->id) }}" data-popup="tooltip" data-placement="bottom" data-original-title="Edit Item"><i class="icon-pencil"></i></a>

                        <a data-toggle="modal" data-target="#modal_theme_warning" class="btn bg-danger btn-icon rounded-round delete_item" link="{{route('item.delete',$value->id)}}" data-popup="tooltip" data-placement="bottom" data-original-title="Delete"><i class="icon-bin"></i></a>

                    </td>

            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="5">No Item Found !!!</td>
            </tr>
            @endif
        </tbody>

    </table>
</div>
<div class="col-12">
    <span class="float-right pagination align-self-end mt-3">
        {{ $item->links() }}
    </span>
</div>
</div>

 <!-- Warning modal -->
    <div id="modal_theme_warning" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                 <div class="modal-body">
                    <center>
                        <i class="icon-alert text-danger icon-3x"></i>
                    </center>
                    <br>
                    <center>
                        <h2>Are You Sure Want To Delete ?</h2>
                        <a class="btn btn-success get_link" href="">Yes, Delete It!</a>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
<!-- /warning modal -->

<script type="text/javascript">
    $('document').ready(function() {

        $('.delete_item').on('click', function() {
            var link = $(this).attr('link');
            $('.get_link').attr('href', link);
        });
    });

</script>

@endsection

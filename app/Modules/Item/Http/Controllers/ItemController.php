<?php

namespace App\Modules\Item\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use App\Modules\Item\Repositories\ItemInterface;

class ItemController extends Controller
{
     protected $item;
    
    public function __construct(ItemInterface $item)
    {
        $this->item = $item;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $search = $request->all();
        $data['item'] = $this->item->findAll($limit= 50, $search); 
        $data['search_value']=$search;
        return view('item::item.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {  
        return view('item::item.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         $data = $request->all();

         try{ 

            $this->item->save($data); 
           
            toastr()->success('Item Created Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('item.index'));
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $data['item'] = $this->item->find($id);    
        return view('item::item.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

         try{ 
            
            $this->item->update($id,$data);
            
            toastr()->success('Item Updated Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        
        return redirect(route('item.index'));
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
       try{
            $this->item->delete($id);
            toastr()->success('Item Deleted Successfully');
        }catch(\Throwable $e){
            toastr()->error($e->getMessage());
        }
        return redirect(route('item.index'));  
    }

}

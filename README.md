<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## System Setup and About Reward CRM
	
	Please Follow following Step to setup system.

	1. After cloning Files, Run composer install.
	2. Then, create file .env and configuration databases. [replace .env.examples to .env file :: Also Change value of DB_DATABASE,DB_USERNAME and DB_PASSWORD ]
	3. Migrate & Seed :  php artisan module:migrate and php artisan module:seed
	4. Run Localhost Environment : php artisan serve
	5. url :: http://localhost:8000
	6. Enter Username & Password :: demo@demo.com / demo@demo.com
	7. At Dashboard Overview , There is Reward Button. You can click and Review Reward Modules. http://localhost:8000/admin/reward



## System Flow. 

1. First Add Items As Much as you can :: http://localhost:8000/admin/item
2. Then After Order Set From Customer :: http://localhost:8000/admin/order
3. Once the Order is Set. Change Status from Open to Completed.
4. Once the Status is Completed, Linked Customer will have Point and Point Amt.
5. To View Points, Go to Customer Section :: http://localhost:8000/admin/customer


## Project Life Cycle Explaination + Test Question 

1. Flowchart or UML diagram on the reward system
https://gitlab.com/sndrawal/reward-crm/-/blob/master/Reward_crm_flowchart.jpeg

2.Design MySQL database schema for this reward system
https://gitlab.com/sndrawal/reward-crm/-/blob/master/er_diagram_reward.png


## Customer Point + Amount Flow

1. At First, When the Order is placed with New Customer will set with New Point and Amount after the Order Completion(Status == Completed).
2. Then After, when Re-order with same customer, then Customer can use his point Amount. But there will be one more Condition.
  
2.1. If Expiry Date is remaining, then he/she can use Point Amount otherwise it will be 0.

Example for Clearity
Customer A buy 'abc' items with worth $ 50.

A :: Point -> 50 and Point Amount will be 0.05(50/0.01) and Expiry Date will be 1year plus from order date.

Customer A buy again 'abc' or 'xyz' item with worth $ 150. but point amount will be use here (0.05)

Then,

A :: Point -> 50 +150 = 200 and Point amount will be 0.15 only.  As previous point amount was used already.

Next Scenario, 
Customer A buy 'ttt' item with worth $200. but point amount is expiry. then point amount won't be deducated in Total Order sales. can't use point amount any more.
So,
A:: Point -> 200+200 = 400 and point amount will be 0.02 with New Expiry Date set.



## Question 2: MySQL Query 

For Mysql, You can check in Order Modules : Here is the url
https://gitlab.com/sndrawal/reward-crm/-/blob/master/app/Modules/Order/Repositories/ItemOrderRepository.php

Note: To Check Order Query :: Please check  Order/Repositories/ItemOrderRepository.php and  Order/Repositories/OrderRepository.php

Same as For Customer and Items :: app/Modules/Customer/Repositories/CustomerRepository.php   and  app/Modules/Item/Repositories/ItemRepository.php


## Question 3 : Calculation

Most of the Calculation is done on Order Controller. Here is the Url
https://gitlab.com/sndrawal/reward-crm/-/blob/master/app/Modules/Order/Http/Controllers/OrderController.php


Note : I try to Explain as much as possible from my side. If there is any confusion regarding code Review or flow. Please contact me via phone or Email


Best Regards, 

Shyam Sundar Awal
sndrawal50@gmail.com
+977 9808500261
Nepal